import * as firebase from 'firebase';
// import 'firebase/auth';
// import 'firebase/database';

  // Initialize Firebase
  var config = {
    apiKey: "AIzaSyCourHVLPHvz7NwQ30r19DeNRXDjSiDp8w",
    authDomain: "curso-fb-camila.firebaseapp.com",
    databaseURL: "https://curso-fb-camila.firebaseio.com",
    projectId: "curso-fb-camila",
    storageBucket: "curso-fb-camila.appspot.com",
    messagingSenderId: "1019722253169"
  };
  firebase.initializeApp(config);

  if( !firebase.apps.length ) {
    firebase.initializeApp( config )
  }
export const auth = firebase.auth();
export const database = firebase.database();
export const storage = firebase.storage();
export default firebase;
