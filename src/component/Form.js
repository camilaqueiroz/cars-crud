import React, { Component } from 'react';
import firebase, { database, storage } from '../services/firebase';
import { setTimeout } from 'core-js';

class Form extends Component {

  constructor() {
    super();
    this.state = {
      statusSetMessage: '',
    }
  }

  uploadPhoto() {
  var file = this.photo.files[0];
   if(file) {
    var metadata = {
      contentType: 'image/jpeg',
    }
    const refStorage = storage.ref();
    var uploadTask = refStorage.child('images/' + this.photo.value.replace(/C:\\fakepath\\/i, '')).put(file, metadata);

    console.info(uploadTask.on('state_changed'))
    uploadTask.on('state_changed', (snapshot) =>{
      // Observe state change events such as progress, pause, and resume
      // Get task progress, including the number of bytes uploaded and the total number of bytes to be uploaded
      var progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
      console.log('Upload is ' + progress + '% done');
      switch (snapshot.state) {
        case firebase.storage.TaskState.PAUSED: // or 'paused'
          console.log('Upload is paused');
          break;
        case firebase.storage.TaskState.RUNNING: // or 'running'
          console.log('Upload is running');
          break;
      }
    }, (error) => {
      console.info('error ' + error)
    }, () => {
      // Handle successful uploads on complete
      // For instance, get the download URL: https://firebasestorage.googleapis.com/...
      uploadTask.snapshot.ref.getDownloadURL().then( (downloadURL) => {
        console.log('File available at', downloadURL);
        this.urlPhoto = downloadURL;
        this.pushOffer(downloadURL)
      });
    });
   }
  }

 pushOffer(urlPhoto) {
    const refDb = database.ref('/offers');
      refDb.push()
        .then(response => {
          response.set({
            brand: this.brand.value.toLowerCase(),
            model: this.model.value.toLowerCase(),
            year: this.year.value,
            price: this.price.value,
            photo: urlPhoto,
            id: response.key,
          }, error => {
            if(error) {
              document.querySelectorAll('input').forEach(input => {
                input.value = ''
              })
              this.setState({statusSetMessage: 'Erro'})
              this.erroTeste();
            } else {
              document.querySelectorAll('input').forEach(input => {
                input.value = ''
              })
              this.setState({statusSetMessage: 'Sucesso'})
              this.erroTeste();
            }
          })
        })
  }
  sendToBd(event) {
    event.preventDefault();

    this.uploadPhoto();
    
    
  }

  erroTeste() {
    if(this.state.statusSetMessage === 'Sucesso') {
      this.status.classList.add('is-success')
      this.status.classList.remove('is-hidden')
      setTimeout(() => {
        this.status.classList.add('is-hidden')
      }, 4000);
    } else {
      this.status.classList.add('is-danger')
      this.status.classList.remove('is-hidden')
      setTimeout(() => {
        this.status.classList.add('is-hidden')
      }, 4000);
    }
  }

  render() {
    return (
      <form className="column is-4" onSubmit={this.sendToBd.bind(this)}>
            
        <div className="field">
          <label className="label">Marca</label>
          <div className="control">
            <input className="input" type="text" placeholder="Marca do veículo" ref={input => this.brand = input} />
          </div>
        </div>

        <div className="field">
          <label className="label">Modelo</label>
          <div className="control">
            <input className="input" type="text" placeholder="Modelo do veículo" ref={input => this.model = input} />
          </div>
        </div>

        <div className="field">
          <label className="label">Ano</label>
          <div className="control">
            <input className="input" type="number" placeholder="Ano do veículo" ref={input => this.year = input} />
          </div>
        </div>

        <div className="field">
          <label className="label">Valor</label>
          <div className="control">
            <input className="input" type="number" placeholder="valor do veículo" ref={input => this.price = input} />
          </div>
        </div>

        <div className="field">
          <label className="label">Foto</label>
          <div className="control">
            <input className="input" type="file" ref={input => this.photo = input} />
          </div>
        </div>

        <span className="message is-hidden" ref={messageStatusError => this.status = messageStatusError}>
          <div className="message-header">
            <p>
              { this.state.statusSetMessage }
            </p>
          </div>
        </span>

        <button type="submit" className="button is-primary">Gravar</button>

      </form>
    );
  }
}

export default Form;