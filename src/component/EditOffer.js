import React, {Component} from 'react';
import { database } from '../services/firebase';

class EditOffer extends Component {
  constructor(props) {
    super(props);
  }
  editForm(event) {
    event.preventDefault();
    database.ref('offers/' + this.props.offerEditable.id).update({
      brand: this.brand.value !== '' ? this.brand.value : this.props.offerEditable.brand,
      model: this.model.value !== '' ? this.model.value : this.props.offerEditable.model,
      year: this.year.value !== '' ? this.year.value : this.props.offerEditable.year,
      price: this.price.value !== '' ? this.price.value : this.props.offerEditable.price,
      id: this.props.offerEditable.id,
      photo: this.props.offerEditable.photo,
    }, function(error) {
      if (error) {
        console.info('error ' + error)
      } else {
          document.querySelectorAll('input').forEach(input => {
            input.value = ''
          })
        }
      });
  }
  
  render() {
    return(
      <form className="column is-4" onSubmit={this.editForm.bind(this)}>
            
        <div className="field">
          <label className="label">Marca</label>
          <div className="control">
            <input className="input" type="text" placeholder="Marca do veículo" ref={input => this.brand = input} />
          </div>
        </div>

        <div className="field">
          <label className="label">Modelo</label>
          <div className="control">
            <input className="input" type="text" placeholder="Modelo do veículo" ref={input => this.model = input} />
          </div>
        </div>

        <div className="field">
          <label className="label">Ano</label>
          <div className="control">
            <input className="input" type="number" placeholder="Ano do veículo" ref={input => this.year = input} />
          </div>
        </div>

        <div className="field">
          <label className="label">Valor</label>
          <div className="control">
            <input className="input" type="number" placeholder="valor do veículo" ref={input => this.price = input} />
          </div>
        </div>

        <div className="field">
          <label className="label">Foto</label>
          <div className="control">
            <input className="input" type="file" ref={input => this.photo = input} />
          </div>
        </div>

        <button type="submit" className="button is-primary">Editar</button>

      </form>
    )
  }
}

export default EditOffer;