import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class Card extends Component {
  render() {
    return(
      <div className="column is-4">
        <div className="card">
          <div className="card-image">
            <figure className="image is-4by3">
              <img src={this.props.offer.photo} alt="Placeholder image" />
            </figure>
          </div>
          <div className="card-content">
            <div className="media">
              <div className="media-left">
                <figure className="image is-48x48">
                  <img src="https://bulma.io/images/placeholders/96x96.png" alt="Placeholder image" />
                </figure>
              </div>
              <div className="media-content">
                <p className="title is-4">John Smith</p>
                <p className="subtitle is-6">@johnsmith</p>
              </div>
            </div>

            <div className="content">
              <h4>{this.props.offer.brand} {this.props.offer.model} {this.props.offer.year}</h4>
              <h5>{this.props.offer.price}</h5>
              <Link to={`/details/${this.props.offer.id}`}>Ver mais</Link>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default Card;