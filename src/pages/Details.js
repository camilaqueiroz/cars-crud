import React, { Component } from 'react';
import { database } from '../services/firebase';

import EditOffer from '../component/EditOffer';

class Details extends Component {
  constructor(props) {
    super(props);
    this.props = props;
    this.state = {
      offer: {}
    }
  }
  componentDidMount() {
    database.ref(`/offers/${this.props.match.params.id}`).on('value', snapshot => {
      if(snapshot.exists()) {
        this.setState({offer: snapshot.val()});
      }
    })
  }
  render() {
    return(
      <div>
        <h1 className="title is-1">{ this.state.offer.brand } { this.state.offer.model } { this.state.offer.year } { this.state.offer.price }</h1>
        <img src={this.state.offer.photo} />
        <EditOffer offerEditable={this.state.offer} />
      </div>
    )
  }
}

export default Details;