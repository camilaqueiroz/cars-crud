import React, { Component } from 'react';
import {database} from '../services/firebase';

import Card from '../component/Card';

class CarsList extends Component {
  constructor() {
    super();
    this.state = {
      offers: []
    }
  }
  componentDidMount() {
    database.ref('/offers').on('value', snapshot => {
      if(snapshot.exists()) {
        let index = 0;
        snapshot.forEach((child) => {
          let cloneState = this.state.offers
          cloneState[index] = child.val();
          this.setState({offers: cloneState})
          index++;
        })
      }
    })
  }
  render() {
    return(
      <div className="container">
        <div className="columns is-multiline">
          <div className="column is-12">
            <h1 className="title is-1">Cars List</h1>
          </div>
          {
            this.state.offers.map(offer => 
              <Card offer={offer} />
            )
          }
        </div>
      </div>

    );
  }
}

export default CarsList;