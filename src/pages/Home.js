import React, { Component } from 'react';

import Form from '../component/Form';
import Card from '../component/Card';
import { database } from '../services/firebase';

class Home extends Component {
  constructor() {
    super();
    this.state = {
      offers: []
    }
  }
  componentDidMount() {
    database.ref('/offers').on('value', snapshot => {
      if(snapshot.exists()) {
        let index = 0;
        snapshot.forEach((child) => {
          let cloneState = this.state.offers
          cloneState[index] = child.val();
          this.setState({offers: cloneState})
          index++;
        })
        this.setState({offers: this.state.offers.reverse().slice(0,6)})
      }
    })
  }

  render() {
    return (
      <div className="container">
        <div className="columns">
          <Form></Form>
          <div className="column is-8">
            <div className="columns is-multiline">
              {
                this.state.offers.map( offer => 
                  <Card key={offer.id} offer={offer}></Card>
                )
              }
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Home;