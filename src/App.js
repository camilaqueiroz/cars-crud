import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom';
import './assets/css/style.scss';
import Navbar from './component/Navbar';
import Home from './pages/Home';
import CarsList from './pages/CarsList';
import Details from './pages/Details';

class App extends Component {
  render() {
    return (
      <div className="wrapperBody">
        <header>
          <Navbar />
        </header>
        <main>
          <Switch>
            <Route exact path='/' component={Home} />
            <Route exact path='/list' component={CarsList} />
            <Route path='/details/:id' component={Details} />
          </Switch>
        </main>
      </div>
    );
  }
}

export default App;
